#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <si/shunyaInterfaces.h>
#include<json-c/json.h> 

int main()
{
FILE *fp;
char buffer[1024];
struct json_object *parsed_json;
struct json_object *Vibration;
struct json_object *val;
struct json_object *Sound;
struct json_object *Temperature;
struct json_object *Timestamp;
struct json_object *Battery;
	
size_t n_Vibration;
size_t i;
	
fp = fopen("/etc/shunya/data.json","r");
fread(buffer, 1024, 1, fp);
fclose(fp);

parsed_json = json_tokener_parse(buffer);

json_object_object_get_ex(parsed_json, "Vibration", &Vibration);
json_object_object_get_ex(parsed_json, "Sound", &Sound);
json_object_object_get_ex(parsed_json, "Temperature", &Temperature);
json_object_object_get_ex(parsed_json, "Timestamp", &Timestamp);
json_object_object_get_ex(parsed_json, "Battery", &Battery);
	
int sd = json_object_get_int(Sound);
int tmp = json_object_get_int(Temperature);
int ts = json_object_get_int(Timestamp);
int bt = json_object_get_int(Battery); 


n_Vibration = json_object_array_length(Vibration);
int value;
int arr[3];
for(i=0;i<n_Vibration;i++) {
val = json_object_array_get_idx(Vibration, i);
i+1;		
value = json_object_get_int(val);
arr[i] = value;}


mqttObj broker1 = newMqtt("mqtt");
mqttConnect(&broker1);
mqttPublish(&broker1,"motor_data","Vibration: %d\n%d\n%d\nSound: %d\nTemperature: %d\nTimestamp: %d\nBattery: %d\n",arr[0],arr[1],arr[2],sd,tmp,ts,bt) ;
mqttDisconnect(&broker1);

return 0;
}