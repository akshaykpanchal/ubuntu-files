/** @file   HTTP.c
 *	@brief
 *   This file includes a C code to connect HTTP server(influxDB) and send data.
 *  This code simply fetches a motor anomaly data which is stored in a JSON file
 *	JSON file is stored in a local folder of a device
 *	And Sends the data of that JSON file to a specified HTTP server on a particular topic


 *	@author Prince Bhalawat
 *	Gitlab - Prince27
*/

/*####################################################################################################################*/

/** ---- Includes ---- */
/* ------------------- */

/** Standard includes*/
#include<stdio.h> // for Standard Input Output
#include<string.h> // for manipulating C strings and arrays
#include<stdlib.h> // for memory allocation, process control,conversions and others

/** Shunya interfaces header file*/
/** for sending JSON data through the http server*/
#include <si/shunyaInterfaces.h>

/* JSON parsing library */
#include<json-c/json.h> // to extract the data from JSON file in respective data type

/*####################################################################################################################*/


struct data{
int vs;
int sds;
int tmps;
int tss;
int bts;};

/* Initialization of function of reading data */
int read_data();

/** @brief Main function
*
*	This establishes an connection with HTTP server
*	and sends a data to a particular topic
*
*	@return does not return anything
*/
int main()
{
influxdbObj testdb = newInfluxdb("test-db");//Creates a new Influxdb Instance and returns influxdbObj Instance for Influxdb
writeDbInflux (testdb, "%d",read_data());//Send Data to Influx DB.
return 0; // return 0 function as our main function is type integer
}

/** @brief Function to read JSON file data stored in the local storage of the device
*
*	This function stores the json file data into buffer and converts
*	it from object file element to int type simple number
*
*	@return returns parsed data from json file into a struct from
*/
struct data read_data()
{
FILE *fp; // pointer variable to read file
char buffer[1024]; // buffer to store contents of JSON file
struct json_object *parsed_json; // structure to store entire JSON document
struct json_object *Vibration; // for storing Vibration data
struct json_object *val; // for storing each item of Vibration array
struct json_object *Sound; // for storing sound data
struct json_object *Temperature; // for storing Temperature data
struct json_object *Timestamp; // for storing timestamp
struct json_object *Battery; // for storing battery percentage

size_t n_Vibration; // varibale to store size of vibration array
size_t i; // counter varibale for FOR loop

fp = fopen("/etc/shunya/data.json","r"); // pointer to file location in local directory
fread(buffer, 1024, 1, fp);  // to read file contents and store it into buffer
fclose(fp); // closes file

parsed_json = json_tokener_parse(buffer); // to parse json file contents and convert it into json object


json_object_object_get_ex(parsed_json, "Vibration", &Vibration);
json_object_object_get_ex(parsed_json, "Sound", &Sound);
json_object_object_get_ex(parsed_json, "Temperature", &Temperature);
json_object_object_get_ex(parsed_json, "Timestamp", &Timestamp);
json_object_object_get_ex(parsed_json, "Battery", &Battery);


int sd = json_object_get_int(Sound);
int tmp = json_object_get_int(Temperature);
int ts = json_object_get_int(Timestamp);
int bt = json_object_get_int(Battery);

struct data data1 = { .sds = sd, .tmps = ts, .tss = ts, .bts = bt };

n_Vibration = json_object_array_length(Vibration);
int value;
int arr[2];
for(i=0;i<n_Vibration;i++) {
val = json_object_array_get_idx(Vibration, i);
i+1;
value = json_object_get_int(val);
arr[i] = value;
data1.vs = arr[i];}

return data1;

}
