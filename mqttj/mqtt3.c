/** @file   MQTT.c 
 *	@brief  This file includes a C code to send motor anomaly data to MQTT broker
 *	
 *	This code simply fetches a motor anomaly data which is stored in a JSON file	
 *	JSON file is stored in a local folder of a device
 *	And Sends the data of that JSON file to a specified MQTT broker on a perticular topic
 *	
 *	@author Akshay Panchal	
 *	Gitlb - akshaykpanchal
*/

/*####################################################################################################################*/

/** ---- Includes ---- */
/* ------------------- */ 

/** Standard includes*/
#include<stdio.h> // for Standard Input Output
#include<string.h> // for manipulating C strings and arrays
#include<stdlib.h> // for memory allocation, process control,conversions and others

/** Shunya interfaces header file*/
/** for sending JSON data to the MQTT broker using Shunya interfaces APIs */
#include <si/shunyaInterfaces.h>

/* JSON parsing library */
#include<json-c/json.h> // to extract the data from JSON file in respective data type 

/*####################################################################################################################*/

/** @brief Main function
*	
*	Fetches JSON file data from local storage of the device and stores
*	it into different variables declared for sotring values of each 
*   parameter
*   
*	@return does not return anything
*/
int main()
{
FILE *fp; // pointer variable to read file 
char buffer[1024]; // buffer to store contents of JSON file
struct json_object *parsed_json; // structure to store entire JSON document
struct json_object *Vibration; // for storing Vibration data
struct json_object *val; // for storing each item of Vibration array
struct json_object *Sound; // for storing sound data   
struct json_object *Temperature; // for storing Tempreture data
struct json_object *Timestamp; // for storing timestamp
struct json_object *Battery; // for storing battery percentage
	
size_t n_Vibration; // varibale to store size of vibration array
size_t i; // counter varibale for FOR loop
	
fp = fopen("/etc/shunya/data.json","r"); // pointer to file location in local directory
fread(buffer, 1024, 1, fp);  // to read file contents and store it into buffer
fclose(fp); // closes file

parsed_json = json_tokener_parse(buffer); // to parse json file contents and convert it into json object

/** functions to get values of perticular keys of the json file */
json_object_object_get_ex(parsed_json, "Vibration", &Vibration); // to get value of key Vibration
json_object_object_get_ex(parsed_json, "Sound", &Sound); // to get value of Sound
json_object_object_get_ex(parsed_json, "Temperature", &Temperature); // to get value of Tempreture
json_object_object_get_ex(parsed_json, "Timestamp", &Timestamp); // to get timestamp
json_object_object_get_ex(parsed_json, "Battery", &Battery); // to get battery perentage
	
/** This sotres the values into a variables */ 
int sd = json_object_get_int(Sound); // function stores the value of sound key into sd variable
int tmp = json_object_get_int(Temperature); // stores value of temperature into tmp
int ts = json_object_get_int(Timestamp); // timestamp to variable ts
int bt = json_object_get_int(Battery);  // battery percentage to bt

n_Vibration = json_object_array_length(Vibration); //to get length of array vibration
int value; // value variable to store value of vibration key
int arr[2]; // array to store vibration data (user can change array size as per requirement)

/** this for loop fetches one by one vibration key values
and stores it into arr array */
for(i=0;i<n_Vibration;i++) {
val = json_object_array_get_idx(Vibration, i); // this fetches how many values are there to be sotred in array arr
i+1; // increment i in order to increment index of array arr 
value = json_object_get_int(val); // stores the values of vibration into value variable
arr[i] = value; // stores the values of vibration key one by one into a arr array
}
   

mqttObj broker1 = newMqtt("mqtt"); //creates new instance with MQTT broker
mqttConnect(&broker1);  //connects to the MQTT broker
/** publishes a data to MQTT broker on motor_data topic */
mqttPublish(&broker1,"motor_data","Vibration: %d\nSound: %d\nTemperature: %d\nTimestamp: %d\nBattery: %d\n",arr[i],sd,tmp,ts,bt) ;
mqttDisconnect(&broker1); //releases connection with MQTT broker

return 0;
}