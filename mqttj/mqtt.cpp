#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <si/shunyaInterfaces.h>


/*char* read_data()
{
    using namespace std;

    std::ifstream myFile("/etc/shunya/data.json");
    std::ostringstream tmp;
    tmp << myFile.rdbuf();
    std::string motordata = tmp.str();
    

    mqttObj broker1 = newMqtt("mqtt");
    mqttConnect(&broker1) ;
    mqttPublish(&broker1, "motor_data","%s", motordata.c_str()) ;
    mqttDisconnect(&broker1) ;
}
*/
int main()
{   using namespace std;

    std::ifstream myFile("/etc/shunya/data.json");
    std::ostringstream tmp;
    tmp << myFile.rdbuf();
    std::string motordata = tmp.str();
    

    mqttObj broker1 = newMqtt("mqtt");
    mqttConnect(&broker1) ;
    mqttPublish(&broker1, "motor_data","%s", motordata.c_str()) ;
    mqttDisconnect(&broker1) ;
    return 0;
}

