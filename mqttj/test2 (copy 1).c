#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<json-c/json.h> 


int main(int argc, char **argv) {
	FILE *fp;
	char buffer[1024];
	struct json_object *parsed_json;
	struct json_object *Vibration;
	struct json_object *val;
	struct json_object *Sound;
	struct json_object *Temperature;
	struct json_object *Timestamp;
	struct json_object *Battery;
	
	size_t n_Vibration;
	size_t i;
	
	fp = fopen("d.json","r");
	fread(buffer, 1024, 1, fp);
	fclose(fp);

	parsed_json = json_tokener_parse(buffer);

	json_object_object_get_ex(parsed_json, "Vibration", &Vibration);
	json_object_object_get_ex(parsed_json, "Sound", &Sound);
	json_object_object_get_ex(parsed_json, "Temperature", &Temperature);
	json_object_object_get_ex(parsed_json, "Timestamp", &Timestamp);
	json_object_object_get_ex(parsed_json, "Battery", &Battery);
	
	
	n_Vibration = json_object_array_length(Vibration);
	printf("found %lu values of vibration\n",n_Vibration);
	for(i=0;i<n_Vibration;i++) {
		val = json_object_array_get_idx(Vibration, i);
		printf("%lu. %d\n",i+1,json_object_get_int(val));
	}
	printf("Sound: %d\n", json_object_get_int(Sound));
	printf("Temperature: %d\n", json_object_get_int(Temperature));
	printf("Timestamp: %d\n", json_object_get_int(Timestamp));
	printf("Battery: %d\n", json_object_get_int(Battery));


	

	
	
	
		
}