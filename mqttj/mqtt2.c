#include <iostream>
#include <string>
#include <fstream>
#include <sstream>
#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include <si/shunyaInterfaces.h>
#include<json-c/json.h> 

int main()
{
using namespace std;

std::ifstream myFile("/etc/shunya/data.json");
std::ostringstream tmp;
tmp << myFile.rdbuf();
std::string s = tmp.str();
const char* d = s.c_str();
/*
FILE *fp;
fp = fopen("data.text","w");
fprintf(fp,"%s",d);
fclose(fp);*/

mqttObj broker1 = newMqtt("mqtt");
mqttConnect(&broker1);
mqttPublish(&broker1,"motor_data","%s",d);
mqttDisconnect(&broker1);

    return 0;
}

