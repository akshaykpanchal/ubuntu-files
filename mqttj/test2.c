#include<stdio.h>
#include<string.h>
#include<stdlib.h>
#include<json-c/json.h> 

struct data{
int vs;
int sds;
int tmps;
int tss;
int bts;}; 

int main() {
FILE *fp;
char buffer[1024];
struct json_object *parsed_json;
struct json_object *Vibration;
struct json_object *val;
struct json_object *Sound;
struct json_object *Temperature;
struct json_object *Timestamp;
struct json_object *Battery;
	
size_t n_Vibration;
size_t i;
	
fp = fopen("d.json","r");
fread(buffer, 1024, 1, fp);
fclose(fp);

parsed_json = json_tokener_parse(buffer);

json_object_object_get_ex(parsed_json, "Vibration", &Vibration);
json_object_object_get_ex(parsed_json, "Sound", &Sound);
json_object_object_get_ex(parsed_json, "Temperature", &Temperature);
json_object_object_get_ex(parsed_json, "Timestamp", &Timestamp);
json_object_object_get_ex(parsed_json, "Battery", &Battery);

int sd = json_object_get_int(Sound);
int tmp = json_object_get_int(Temperature);
int ts = json_object_get_int(Timestamp);
int bt = json_object_get_int(Battery); 

struct data data1 = { .sds = sd, .tmps = ts, .tss = ts, .bts = bt };

n_Vibration = json_object_array_length(Vibration);
int value;
int arr[2];
for(i=0;i<n_Vibration;i++) {
val = json_object_array_get_idx(Vibration, i);
i+1;		
value = json_object_get_int(val);
arr[i] = value;
data1.vs = arr[i];}}
